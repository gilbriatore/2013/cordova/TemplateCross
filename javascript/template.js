  function mostrarSmartAndroidVertical(){
		$("#docs").hide();
		$("#aparelho").show();
    var aparelho = document.getElementById("aparelho");
	  aparelho.style.width = "470px";
	  aparelho.style.height = "620px";
	  aparelho.style.backgroundImage="url('imagens/nexus_5_v.png')";
	  aparelho.style.cursor = "pointer";
	
    var tela = document.getElementById("tela");
	tela.style.marginTop = "70px";
	tela.style.marginLeft = "24px";
  	tela.style.width = "274px";
	tela.style.height = "450px";
	tela.style.border = "none";
  }; 
  
  function mostrarSmartAndroidHorizontal(){
		$("#docs").hide();
		$("#aparelho").show();	
    var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "670px";
	aparelho.style.height = "330px";
	aparelho.style.backgroundImage="url('imagens/nexus_5_h.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "30px";
	tela.style.marginLeft = "70px";
  	tela.style.width = "450px";
	tela.style.height = "274px";
	tela.style.border = "none";
  }; 
  
  function mostrarTabletAndroidHorizontal(){
		$("#docs").hide();
		$("#aparelho").show();			
    var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "855px";
	aparelho.style.height = "580px";
	aparelho.style.backgroundImage="url('imagens/nexus_10_h.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "73px";
	tela.style.marginLeft = "79px";
  	tela.style.width = "643px";
	tela.style.height = "421px";
	tela.style.border = "none";	
  };
  
  function mostrarTabletAndroidVertical(){
 		$("#docs").hide();
		$("#aparelho").show();	
		var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "640px";
	aparelho.style.height = "805px";
	aparelho.style.backgroundImage="url('imagens/nexus_10_v.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "78px";
	tela.style.marginLeft = "73px";
  	tela.style.width = "421px";
	tela.style.height = "643px";
	tela.style.border = "none";	
  }; 
  
   function mostrarSmartiPhoneVertical(){
 		$("#docs").hide();
		$("#aparelho").show();	
		 var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "450px";
	aparelho.style.height = "600px";
	aparelho.style.backgroundImage="url('imagens/iphone_v.png')";
	aparelho.style.cursor = "pointer";
	
    var tela = document.getElementById("tela");
	tela.style.marginTop = "112px";
	tela.style.marginLeft = "37px";
  	tela.style.width = "250px";
	tela.style.height = "372px";
	tela.style.border = "none";		
  }; 
  
  function mostrarSmartiPhoneHorizontal(){
 		$("#docs").hide();
		$("#aparelho").show();	
		var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "670px";
	aparelho.style.height = "320px";
	aparelho.style.backgroundImage="url('imagens/iphone_h.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "38px";
	tela.style.marginLeft = "112px";
  	tela.style.width = "372px";
	tela.style.height = "250px";
	tela.style.border = "none";	
  }; 
  
   function mostrarTabletIpadHorizontal(){
 		$("#docs").hide();
		$("#aparelho").show();	
		 var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "855px";
	aparelho.style.height = "625px";
	aparelho.style.backgroundImage="url('imagens/ipad_h.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "86px";
	tela.style.marginLeft = "85px";
  	tela.style.width = "580px";
	tela.style.height = "425px";
	tela.style.border = "none";		
  };
  
  function mostrarTabletIpadVertical(){
 		$("#docs").hide();
		$("#aparelho").show();	
		var aparelho = document.getElementById("aparelho");
	aparelho.style.width = "695px";
	aparelho.style.height = "805px";
	aparelho.style.backgroundImage="url('imagens/ipad_v.png')";
	aparelho.style.cursor = "pointer";
	
	var tela = document.getElementById("tela");
	tela.style.marginTop = "95px";
	tela.style.marginLeft = "75px";
  	tela.style.width = "436px";
	tela.style.height = "570px";
	tela.style.border = "none";		
  };  

  function mostrarDocs(){
    $("#conteudo").css("background-color","white");
		$("#aparelho").hide();
		$("#aparelhos").hide();
		$("#docs").show();	
  };  
  
  
